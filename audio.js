var audioManager = {
    sounds      : {},
    soundClass  : null,
    cookieName  : "audioManager",
    cookieExp   : new Date(new Date().getTime() + (60*60*24*7*1000)),
    init: function (){
        var me = this;
        window.AudioContext = window.AudioContext||window.webkitAudioContext;
        if(window.AudioContext){
            me.soundClass = soundObj;
        }else{
            me.soundClass = audioTag;
        }

	if($.cookie){
            var sounds = JSON.parse($.cookie(me.cookieName));
            me.applySavedValues (sounds);	
	}
        me.saveChanges();
    },
    saveChanges : function(){
        var me      = this,
            data    = {},
            spec    = null;
        for (var name in me.sounds){
            spec = me.getSoundSpecification(name)
            if (spec){
                data[name] = spec;
            }
        }
	if($.cookie){
            $.cookie(me.cookieName, JSON.stringify( data ), {expires: me.cookieExp});
        }
    },
    applySavedValues : function(values){
        var me      = this,
            values  = values ? JSON.parse(values) : null,
            data,
            sound,
            spec;
            
        if(values){
            for (var name in values){
                sound = me.add(name);
                data  = values[name];
                for (spec in data){
                    sound[spec] = data[spec];
                }
            }
        }
    },
    add : function(name){
        var me = this;
        if(!me.sounds[name]){
            me.sounds[name] =  new me.soundClass(name, me);
        }
        me.saveChanges();
        return me.sounds[name];
    },
    get : function(name){
        var me = this;
        if (me.sounds[name]){
            return me.sounds[name];
        }
        else{
            return null;
        }
    },
    del : function(name){
        var me = this;
        if (me.sounds[name]){
            me.sounds[name].stop();
            me.sounds[name].close();
            delete(me.sounds[name]);
        }
    },
    getSoundSpecification : function(name){
        var me   = this,
            spec = {};
    
        var sound = me.get(name);
        
        if (sound){
            spec  = {
                muteStatus  : sound.muteStatus,
                volume      : sound.volume,
                playing     : sound.playing,
                loop        : sound.loop
            };
        }
        
        return spec;
    }
}

var soundObj = function(name, parent){
    this.context    = new window.AudioContext();
    this.parent     = parent;
    this.name       = name;
    this.muteStatus = false;
    this.volume     = 0.5;
    this.buffer     = null;
    this.source     = null;
    this.gainNode   = null;
    this.playing    = false;
    this.loop       = false;
    this.loadFromUrl= function(url){
        var me =this,
            request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.responseType = 'arraybuffer';

        // Decode asynchronously
        request.onload = function() {
          me.context.decodeAudioData(request.response, function(buffer) {
            me.buffer = buffer;
          });
          if(me.playing){
            me.play();
          }
        };
        request.send();
        me.parent.saveChanges();
        return me;
    };
    this.play = function (time) {
        var me      =this,
            time    = isFinite(time)? time : 0;    
        if(! me.buffer){
            return false;
        }
        if(! me.source || me.source.playbackState != 0){
            try{
                if(me.source.playbackState != 0){
                    me.close();
                }
            }catch(e){
                console.warn(e);
            }
            
            try{
                me.gainNode = me.context.createGain();
                me.gainNode.connect(me.context.destination);
                me.gainNode.gain.value = me.volume;
                me.source =  me.context.createBufferSource();
                me.source.connect(me.gainNode);
                me.source.buffer = me.buffer;
                me.source.loop   = me.loop;
            }catch(e){
                console.warn(e);
            }
        }
        if(!me.muteStatus){
            me.source.start(time);
        }
        return true;
    };
    this.setVolume = function(volume){
        var me = this;
        if(volume>=0 && 1>=volume ){
            if(me.gainNode){
                me.gainNode.gain.value = volume;
            }
            me.volume              = volume;
            me.parent.saveChanges();
            return true;
        }else{
            return false;
        }
    };
    this.getVolume = function(){
        var me = this;
        return me.volume * 100;
    };
    this.setLoop = function(loopStatus){
        var me   = this,
            loop = loopStatus ? true : false ;
        me.loop = loop;
        if(me.source)
            me.source.loop = loop;
        me.parent.saveChanges();
        return me;
    };
    this.stop = function(){
        var me   = this;
        if(me.source && me.source.stop){
            me.source.stop(0);
        }else if(me.source){
            me.source.stop = me.source.noteOff;
            me.source.stop(0);
        }
        return me;
    };
    this.close = function(){
        var me   = this;
        if(me.source){
            me.source.disconnect(0);
        }
        if(me.gainNode){
            me.gainNode.disconnect(0);
        }
        return me;
    };
    this.setPlayStatus = function (status){
        var me   = this,
            status = status ? true : false;
        me.playing  = status;
        if(status){
            me.muteStatus = false;
            me.play();
        }else{
            me.stop();
        }
        me.parent.saveChanges();
        return me;
    };
    this.mute = function(el){
        var me = this;
        me.muteStatus= true;
	if(el){
            try{
                $(el).disable();
            }catch(e){}
        }
        me.stop();
        me.parent.saveChanges();
        return me;
    };
    this.unmute = function(el){
        var me = this;
        me.muteStatus= false;
        
	if(el){
	     try{
	         $(el).enable();
	     }catch(e){}
	}
        if(me.playing){
            me.play();
        }
        me.parent.saveChanges();
        return me;
    };
    this.toggleMute = function(btn){
        var me = this;
        if(me.muteStatus){
            me.unmute(btn);
            if(btn) $(btn).addClass("AlarmSound");
            if(btn) $(btn).removeClass("AlarmMute");
        }else{
            me.mute(btn);
            if(btn) $(btn).addClass("AlarmMute");
            if(btn) $(btn).removeClass("AlarmSound");
        }
        return me;
    }
    return this;
}

var audioTag = function(name, parent){
    this.parent     = parent;
    this.element    = null;
    this.volume     = 0.5;
    this.name       = name;
    this.muteStatus = false;
    this.buffer     = null;
    this.source     = null;
    this.gainNode   = null;
    this.playing    = false;
    this.loop       = false;
    this.init       = function(){
        var me = this;
        $("body").append("<audio id='"+ 'soundManager_'+me.name+"' />");
        me.element  = $("#soundManager_"+me.name)[0];
    };
    this.loadFromUrl= function(url){
        var me =this;
        me.element.src = url;
        me.element.load();
        if(me.playing){
            me.play();
        }
        me.parent.saveChanges();
        return me;
    };
    this.play = function (time) {
        var me      =this,
            time    = isFinite(time)? time : 0;    
        
        if(time < me.element.duration){
            me.element.currentTime = time;
        }
        me.setVolume(me.volume);
        me.element.play();
        return true;
    };
    this.setVolume = function(volume){
        var me = this;
        if(volume>=0 && 1>=volume){
            me.element.volume = volume;
            me.volume         = volume;
            return true;
        }else{
            return false;
        }
        me.parent.saveChanges();
    };
    this.getVolume = function(){
        var me = this;
        return me.element.volume * 100;
    };
    this.setLoop = function(loopStatus){
        var me   = this,
            loop = loopStatus ? true : false ;
        me.loop = loop;
        if(me.element)
            me.element.loop = loop;
        me.parent.saveChanges();
        return me;
    };
    this.stop = function(){
        var me   = this;
        me.element.pause();
        me.element.currentTime = 0;
        return me;
    };
    this.close = function(){
        var me   = this;
        me.element.src = "";
        return me;
    };
    this.setPlayStatus = function (status){
        var me   = this,
            status = status ? true : false;
        me.playing  = status;
        if(status){
            me.muteStatus = false;
            me.play();
        }else{
            me.stop();
        }
        me.parent.saveChanges();
        return me;
    };
    this.mute = function(el){
        var me = this;
        me.muteStatus= true;
        if(el){
            try{
                $(el).disable();
            }catch(e){}
        }
        me.stop();
        me.parent.saveChanges();
        return me;
    };
    this.unmute = function(el){
        var me = this;
        me.muteStatus= false;
        if(el){
             try{
                 $(el).enable();
             }catch(e){}
        }
        if(me.playing){
            me.play();
        }
        me.parent.saveChanges();
        return me;
    };
    this.toggleMute = function(btn){
        var me = this;
        if(me.muteStatus){
            me.unmute(btn);
            if(btn) $(btn).addClass("AlarmSound");
            if(btn) $(btn).removeClass("AlarmMute");
        }else{
            me.mute(btn);
            if(btn) $(btn).addClass("AlarmMute");
            if(btn) $(btn).removeClass("AlarmSound");
        }
        return me;
    }
    this.init();
    return this;
}

soundManager.init();
